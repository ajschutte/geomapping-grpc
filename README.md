# Geomapping gRPC version

## Overview 
 
This implements a POC to compare the performance of a service implemented with gRPC + protobuf vs. Jackson + JSON. 
Both service implementations are implemented on Spring Boot. 

The service implementation itself returns dummy data (1000 randomly generated geomappings) 
so as to get a sense of the relative serialization overhead. 

## Running the service via Docker (this is preferred, as it more accurately simulates a remote service)

Pull the Docker image from the Artifactory:

**$ docker pull docker-snapshot-local.artifactory.mgdev.loc/mercurygate/geomapping-grpc-server:1.0.0-SNAPSHOT**

Alternatively, building the Docker image locally from scratch (from the _geomapping-grpc-server_ directory): 

**$ mvn clean install dockerfile:build**

Running the docker image:

**$ docker run -p 8090:8090 -p 7565:7565 docker-snapshot-local.artifactory.mgdev.loc/mercurygate/geomapping-grpc-server:1.0.0-SNAPSHOT**

## Running the service as a Spring Boot app

Alternatively, just run the service as a Spring Boot app locally:

**$ mvn spring-boot:run**

## Executing the clients

There are two test clients in _geomapping-grpc-server/test/java_:

- _GeomappingGrpcControllerClientTest_ - executes simple load tests against the gRPC + protobuf server implementation.

- _GeomappingRestControllerClientTest_ - executes simple load tests against the Jackson + JSON server implementation. 

To point to a particular server where these services are deployed (the default points to _crymesh2_), change the server URL in 
the _initialize()_ method in these test classes. 

To change the load, just change the CLIENT_COUNT parameter defined in these test classes.  

If the services are not running on crymesh2, start the Docker container there with the _docker run_ command specified above. 


