package com.mg.services.entities;

/**
 * Marker for root package containing this project's JPA entity classes.
 */
public @interface JpaEntities {

}
