package com.mg.services.routeguide.api.grpc;

import com.mg.services.routeguide.api.GeoFeature;
import com.mg.services.routeguide.api.GeoFeatureServiceGrpc;
import com.mg.services.routeguide.api.GeoPoint;
import com.mg.services.routeguide.api.GeoRectangle;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.IntStream;

/**
 * @author Andries on 10/26/17.
 */
@GRpcService
public class GeoFeatureGrpcController extends GeoFeatureServiceGrpc.GeoFeatureServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(GeoFeatureGrpcController.class);

    private static final int STREAM_COUNT = 10;

    @Override
    public void createGeoFeature(GeoPoint request, StreamObserver<GeoFeature> responseObserver) {

        logger.info("Invoking createGeoFeature(..)..");

        responseObserver.onNext(checkFeatureFromPoint(request, "dummy"));
        responseObserver.onCompleted();

    }

    @Override
    public void createGeoFeaturesFromRectangle(GeoRectangle request, StreamObserver<GeoFeature> responseObserver) {

        logger.info("Invoking createGeoFeaturesFromRectangle(..)..");

        IntStream.range(0, STREAM_COUNT)
                .mapToObj(i -> checkFeatureFromRectangle(request, "dummy" + i))
                .forEach(responseObserver::onNext);

        responseObserver.onCompleted();

    }

    private GeoFeature checkFeatureFromPoint(GeoPoint point, String name) {

        // return a dummy feature.
        return GeoFeature.newBuilder().setName(name).setPoint(point).build();

    }

    private GeoFeature checkFeatureFromRectangle(GeoRectangle rectangle, String name) {

        // return a dummy feature.
        return GeoFeature.newBuilder().setName(name).setPoint(rectangle.getLoLeftPoint()).build();

    }


}
