package com.mg.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;

/**
 * Entry point for the Geomapping service sprint boot application.
 */
@SpringBootApplication
@ComponentScan(basePackageClasses = { GeomappingRoot.class })
public class GeomappingApplication {

    private static final Logger logger = LoggerFactory.getLogger(GeomappingApplication.class);

    /**
     * Main method for starting the spring boot application.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        logger.debug("Starting up from main(..)..");

        new SpringApplicationBuilder(GeomappingApplication.class)
                .logStartupInfo(true)
                .listeners((ApplicationListener<ApplicationEvent>)(ApplicationEvent applicationEvent) -> logger.debug(
                        "Application Event: {}", applicationEvent
                                .getClass()
                                .getCanonicalName()))
                .run(args);
    }
}
