package com.mg.services.geomapping.api.client;

import com.mg.models.geomapping.Geomapping;
import com.mg.models.geomapping.LocationAddress;
import feign.Headers;
import feign.RequestLine;

import java.util.List;

import static com.mg.api.supported.headers.SupportedHeaders.*;

/**
 * @author Andries on 10/24/17.
 */
public interface GeomappingClient {

    @RequestLine("POST /geomapping/fromLocationAddresses")
    @Headers({
            DEFAULT_ACCEPT_HEADER, DEFAULT_CONTENT_TYPE_HEADER
    })
    List<Geomapping> createGeomappings(List<LocationAddress> locationAddresses);

}
