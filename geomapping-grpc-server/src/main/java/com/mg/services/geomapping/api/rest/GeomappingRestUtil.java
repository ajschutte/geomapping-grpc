package com.mg.services.geomapping.api.rest;

import com.mg.models.geomapping.Geomapping;
import com.mg.models.geomapping.LocationAddress;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Andries on 10/24/17.
 */
public class GeomappingRestUtil {

    public static List<Geomapping> assignRandomGeomappings(List<LocationAddress> locs) {

        List<Geomapping> result = new LinkedList<>();
        for (LocationAddress loc : locs) {
            Geomapping gm = new Geomapping()
                    .withLocationAddress(loc)
                    .withLatitude(ThreadLocalRandom.current().nextDouble(-90.0, +90.0))
                    .withLongitude(ThreadLocalRandom.current().nextDouble(-180.0, +180.0));
            result.add(gm);
        }

        return result;
    }

    public static List<LocationAddress> generateRandomLocationAddresses() {

        List<LocationAddress> result = new LinkedList<>();

        for (int i = 0; i < 200; i++) {
            LocationAddress la = new LocationAddress()
                    .withStreet(randomString())
                    .withCity(randomString())
                    .withStateOrProvince(randomString())
                    .withZipOrPostalCode("" + ThreadLocalRandom.current().nextInt(10000, 99999))
                    .withCountryCode(randomString());

            result.add(la);

        }

        return result;
    }

    private static String randomString() {
        return UUID.randomUUID().toString();
    }

}
