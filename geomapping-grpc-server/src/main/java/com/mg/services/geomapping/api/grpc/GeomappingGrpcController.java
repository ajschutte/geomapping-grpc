package com.mg.services.geomapping.api.grpc;

import com.mg.services.geomapping.api.GeomappingList;
import com.mg.services.geomapping.api.GeomappingServiceGrpc;
import com.mg.services.geomapping.api.LocationAddress;
import com.mg.services.geomapping.api.LocationAddressList;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author Andries on 10/24/17.
 */
@GRpcService
public class GeomappingGrpcController extends GeomappingServiceGrpc.GeomappingServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(GeomappingGrpcController.class);

    private GeomappingList listResponse = null;

    @PostConstruct
    public void init() {

        logger.info("Invoking init()..");

        List<LocationAddress> la = GeomappingGrpcUtil.generateRandomLocationAddresses();

        GeomappingList.Builder responseBuilder = GeomappingList.newBuilder();

        GeomappingGrpcUtil.assignRandomGeomappings(la).forEach(responseBuilder::addGeomapping);

        listResponse = responseBuilder.build();

    }


    @Override
    public void createGeomappings(LocationAddressList request, StreamObserver<GeomappingList> responseObserver) {

        logger.info("Invoking createGeomappings(..)..");

        responseObserver.onNext(listResponse);

        responseObserver.onCompleted();

    }
}
