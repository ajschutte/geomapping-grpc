package com.mg.services.configurations;

/**
 * @author Andries on 8/9/17.
 */
public final class SupportedExecutorServices {

    public static final String SERVICES_EXECUTOR_POOL = "servicesExecutorPool";
    public static final String SCHEDULED_WATCHDOG_EXECUTOR_POOL = "scheduledWatchDogExecutorPool";

    private SupportedExecutorServices() {
    }
}
