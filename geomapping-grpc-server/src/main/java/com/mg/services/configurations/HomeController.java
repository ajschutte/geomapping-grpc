package com.mg.services.configurations;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping(value = "/api-docs")
    public String apidocs() {
        return "redirect:swagger-ui.html";
    }

}
