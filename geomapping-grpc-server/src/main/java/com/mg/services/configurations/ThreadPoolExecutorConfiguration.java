package com.mg.services.configurations;

import com.mg.exceptions.core.ResourceAccessOverloadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mg.services.configurations.SupportedExecutorServices.SCHEDULED_WATCHDOG_EXECUTOR_POOL;
import static com.mg.services.configurations.SupportedExecutorServices.SERVICES_EXECUTOR_POOL;

/**
 * @author Andries on 11/30/16.
 */
@EnableAsync
@Configuration
public class ThreadPoolExecutorConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolExecutorConfiguration.class);

    private static final String THREAD_NAME_PATTERN = "%s-%d";

    private static final String CREATE_LOG_MSG = "Creating Thread Pool: {}";

    @Value("${mg.services.geomapping.schedulerThreadPoolSize}")
    private int schedulerPoolSize;

    @Value("${mg.services.geomapping.coreThreadPoolSize}")
    private int corePoolSize;

    @Value("${mg.services.geomapping.maxThreadPoolSize}")
    private int maxPoolSize;

    @Value("${mg.services.geomapping.maxThreadPoolWaitQueueSize}")
    private int maxQueueSize;

    @Value("${mg.services.geomapping.idleThreadTimeoutSecs}")
    private int idleThreadTimeoutSecs;

    @Value("${mg.services.geomapping.useCustomThreadPool}")
    private boolean useCustomPool;

    @Value("${mg.services.geomapping.servicesThreadPoolNamePrefix}")
    private String servicesPoolPrefix;

    @Value("${mg.services.geomapping.forkJoinParallelismLevel}")
    private int forkJoinParallelismLevel;

    @Bean(name = SERVICES_EXECUTOR_POOL)
    public ExecutorService threadPoolServicesExecutor() {

        logger.debug(CREATE_LOG_MSG, SERVICES_EXECUTOR_POOL);

        return createConfiguredPool(useCustomPool, servicesPoolPrefix, maxQueueSize, corePoolSize, maxPoolSize, idleThreadTimeoutSecs);

    }

    @Bean(name = SCHEDULED_WATCHDOG_EXECUTOR_POOL)
    public ScheduledExecutorService threadPoolScheduledWatchdogExecutor() {

        logger.debug(CREATE_LOG_MSG, SCHEDULED_WATCHDOG_EXECUTOR_POOL);

        return Executors.newScheduledThreadPool(schedulerPoolSize);

    }

    private ExecutorService createConfiguredPool(boolean custom, String threadNamePrefix, int aMaxQueueSize, int aCorePoolSize,
            int aMaxPoolSize, int anIdleTimeoutSecs) {

        ExecutorService executor;

        if (custom) {
            BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(aMaxQueueSize);
            executor = new ThreadPoolExecutor(aCorePoolSize, aMaxPoolSize,
                    anIdleTimeoutSecs, TimeUnit.SECONDS, queue,
                    new LogFriendlyThreadFactory(threadNamePrefix),
                    ThreadPoolExecutorConfiguration::onOverload);
        }
        else {
            if (forkJoinParallelismLevel > 0) {
                executor = new ForkJoinPool(forkJoinParallelismLevel);
            }
            else {
                executor = ForkJoinPool.commonPool();
            }
        }

        return executor;

    }

    private static void onOverload(Runnable r, ThreadPoolExecutor theExecutor) {

        logger.error("Error on Runnable.., Executor overloaded: {} for Runnable: {}",
                theExecutor.getActiveCount(), r);

        throw new ResourceAccessOverloadException(
                String.format("Error on Runnable.., Executor overloaded: %d for Runnable: %s", theExecutor.getActiveCount(), r));

    }

    private static final class LogFriendlyThreadFactory implements ThreadFactory {

        private final String threadNamePrefix;

        private final AtomicInteger counter = new AtomicInteger();

        private LogFriendlyThreadFactory(String threadNamePrefix) {
            this.threadNamePrefix = threadNamePrefix;
        }

        @Override
        public Thread newThread(Runnable r) {
            final String threadName = String.format(THREAD_NAME_PATTERN, threadNamePrefix, counter.incrementAndGet());
            return new Thread(r, threadName);
        }

    }

}
