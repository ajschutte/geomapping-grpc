package com.mg.services.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerDocumentationConfiguration {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Geomapping REST API")
            .description("Geomapping API..")
            .license("Placeholder License")
            .licenseUrl("http://unlicense.org")
            .termsOfServiceUrl("Placeholder TOS")
            .version("v1")
            .contact(new Contact("", "", "andries.schutte@mercurygate.com"))
            .build();
    }

    @Bean
    public Docket customImplementation() {
        Set<String> protocols = new HashSet<>();
        protocols.add("http");
        protocols.add("https");
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("com.mg.services.geomapping"))
                    .build()
                .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(java.time.OffsetDateTime.class, java.util.Date.class)
                .protocols(protocols)
                .apiInfo(apiInfo());
    }

}
