package com.mg.services.configurations;

import com.mg.BaseResourcesCoreRoot;
import com.mg.resources.core.ResourceError;
import com.mg.resources.exceptionhandlers.BaseExceptionHandler;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Andries on 6/15/17.
 */
@Configuration
@ComponentScan(basePackageClasses = { BaseResourcesCoreRoot.class })
public class ExceptionMappersConfiguration {

    /**
     * Must be protected, proxied by Spring..
     */
    protected ExceptionMappersConfiguration() {
    }

    /**
     * This is needed to map Spring Security exceptions..
     */
    @ControllerAdvice
    public static class AccessDeniedExceptionHandler extends BaseExceptionHandler {

        @ResponseBody
        @ExceptionHandler(AccessDeniedException.class)
        @ResponseStatus(HttpStatus.UNAUTHORIZED)
        public ResourceError accessDeniedServerError(AccessDeniedException exx) {

            return createResourceError(HttpStatus.UNAUTHORIZED, exx);

        }

    }

}
