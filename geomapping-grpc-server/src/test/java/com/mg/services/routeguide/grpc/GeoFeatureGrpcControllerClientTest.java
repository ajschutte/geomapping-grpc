package com.mg.services.routeguide.grpc;

import com.mg.services.routeguide.api.GeoFeature;
import com.mg.services.routeguide.api.GeoFeatureServiceGrpc;
import com.mg.services.routeguide.api.GeoPoint;
import com.mg.services.routeguide.api.GeoRectangle;
import com.mg.services.routeguide.api.RouteNote;
import com.mg.services.routeguide.api.RouteServiceGrpc;
import com.mg.services.routeguide.api.RouteSummary;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.stream.IntStream;

/**
 * @author Andries on 10/26/17.
 */
@Ignore
public class GeoFeatureGrpcControllerClientTest {

    private Logger logger = LoggerFactory.getLogger(GeoFeatureGrpcControllerClientTest.class);

    private static final String SERVER_URL = "crymesh2:7565";

    private GeoFeatureServiceGrpc.GeoFeatureServiceStub featureServiceStub;

    private RouteServiceGrpc.RouteServiceStub routeServiceStub;

    private static final int NUM_OF_POINTS = 10;
    private static final int NUM_OF_MESSAGES = 4;
    private static final int NUM_OF_CLIENTS = 3;

    @Before
    public void initialize() {

        ManagedChannel channel = ManagedChannelBuilder
                .forTarget(SERVER_URL)
                .usePlaintext(true) //unencrypted, default uses TLS
                .build();

        featureServiceStub = GeoFeatureServiceGrpc.newStub(channel);
        routeServiceStub = RouteServiceGrpc.newStub(channel);

    }

    @Test
    public void createFeatureNonBlocking() throws Exception {

        GeoPoint request = GeoPoint.newBuilder().setLatitude(21).setLongitude(123).build();

        logger.info("createFeatureNonBlocking() TEST request: {}..", request);

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<GeoFeature> so = new StreamObserver<GeoFeature>() {

            @Override
            public void onNext(GeoFeature value) {
                logger.info("onNext() createFeatureNonBlocking() TEST response: {}..", value);
            }

            @Override
            public void onError(Throwable t) {
                logger.error("onError() createFeatureNonBlocking() TEST..", t);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("onCompleted() createFeatureNonBlocking() TEST..");
                finishLatch.countDown();
            }

        };

        long start = System.currentTimeMillis();

        featureServiceStub.createGeoFeature(request, so);

        finishLatch.await();

        long end  = System.currentTimeMillis();

        logger.info("createFeatureNonBlocking() TEST total time (millis): {}", end - start);

    }

    @Test
    public void listFeaturesNonBlockingServerStreaming() throws Exception {

        GeoRectangle request =
                GeoRectangle.newBuilder()
                        .setLoLeftPoint(GeoPoint.newBuilder().setLatitude(12).setLongitude(123).build())
                        .setHiRightPoint(GeoPoint.newBuilder().setLatitude(14).setLongitude(126).build()).build();

        logger.info("listFeaturesNonBlockingServerStreaming() TEST request: {}..", request);

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<GeoFeature> so = new StreamObserver<GeoFeature>() {

            @Override
            public void onNext(GeoFeature value) {
                logger.info("onNext() listFeaturesNonBlockingServerStreaming() TEST response: {}..", value);
            }

            @Override
            public void onError(Throwable t) {
                logger.error("onError() listFeaturesNonBlockingServerStreaming() TEST..", t);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("onCompleted() listFeaturesNonBlockingServerStreaming() TEST..");
                finishLatch.countDown();
            }

        };

        long start = System.currentTimeMillis();

        featureServiceStub.createGeoFeaturesFromRectangle(request, so);

        finishLatch.await();

        long end  = System.currentTimeMillis();

        logger.info("listFeaturesNonBlockingServerStreaming() TEST total time (millis): {}", end - start);

    }

    @Test
    public void recordRouteNonBlockingClientStreaming() throws Exception {

        logger.info("recordRouteNonBlockingClientStreaming() TEST request..");

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<RouteSummary> so = new StreamObserver<RouteSummary>() {

            @Override
            public void onNext(RouteSummary summary) {

                logger.info("onNext()..TEST..finished trip with {} points. Passed {} features. "
                                + "Travelled {} meters. It took {} seconds.", summary.getPointCount(),
                        summary.getFeatureCount(), summary.getDistanceInMeters(), summary.getElapsedTimeInSecs());

            }

            @Override
            public void onError(Throwable t) {
                logger.error("onError() recordRouteNonBlockingClientStreaming() TEST..", t);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("onCompleted() recordRouteNonBlockingClientStreaming() TEST..");
                finishLatch.countDown();
            }

        };

        long start = System.currentTimeMillis();

        StreamObserver<GeoPoint> ro = routeServiceStub.recordRoute(so);

        Random rand = new Random();

        IntStream.range(0, NUM_OF_POINTS)
                .mapToObj(i -> GeoPoint.newBuilder().setLatitude(i).setLongitude(i).build())
                .forEach(point -> {
                    try {
                        ro.onNext(point);
                        Thread.sleep(rand.nextInt(1000) + 500);
                    }
                    catch (InterruptedException e) {
                        logger.error("Error in recordRouteNonBlockingClientStreaming()", e);
                    }

                });

        ro.onCompleted();

        finishLatch.await();

        long end  = System.currentTimeMillis();

        logger.info("recordRouteNonBlockingClientStreaming() TEST total time (millis): {}", end - start);

    }

    @Test
    public void routeChatNonBlockingBidirectionalStreaming() throws Exception {

        logger.info("routeChatNonBlockingBidirectionalStreaming() TEST request..");

        IntStream.range(0, NUM_OF_CLIENTS).parallel()
                .forEach(i -> {
                    try {
                        spawnClient(i);
                    }
                    catch (Exception e) {
                        logger.error("Error..", e);
                    }
                });

    }

    private void spawnClient(int clientId) throws Exception {

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<RouteNote> so = new StreamObserver<RouteNote>() {

            @Override
            public void onNext(RouteNote note) {

                logger.info("onNext()..TEST..Client ID: {} - received Note Message {}..", clientId, note.getMessage());

            }

            @Override
            public void onError(Throwable t) {
                logger.error("onError() TEST..", t);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("onCompleted() TEST..");
                finishLatch.countDown();
            }

        };

        long start = System.currentTimeMillis();

        StreamObserver<RouteNote> ro = routeServiceStub.routeChat(so);

        Random rand = new Random();

        IntStream.range(0, NUM_OF_MESSAGES)
                .mapToObj(i -> RouteNote.newBuilder().setMessage("HI from Client ID: " + clientId + " with Message ID: " + i).build())
                .forEach(note -> {
                    try {
                        ro.onNext(note);
                        Thread.sleep(rand.nextInt(1000) + 500);
                    }
                    catch (InterruptedException e) {
                        logger.error("Error..", e);
                    }

                });

        ro.onCompleted();

        finishLatch.await();

        long end  = System.currentTimeMillis();

        logger.info("TEST total time (millis): {}", end - start);

    }

}
