package com.mg.services.geomapping.rest;

import com.mg.clients.core.clientbuilders.ApiClientBuilder;
import com.mg.exceptions.core.ResourceAccessExternalServiceFailedException;
import com.mg.models.geomapping.Geomapping;
import com.mg.models.geomapping.LocationAddress;
import com.mg.services.geomapping.api.client.GeomappingClient;
import com.mg.services.geomapping.api.rest.GeomappingRestUtil;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author Andries on 10/24/17.
 */
@Ignore
public class GeomappingRestControllerClientTest {

    private Logger logger = LoggerFactory.getLogger(GeomappingRestControllerClientTest.class);

    private static final int CLIENT_COUNT = 800;

    //Client timeout config..
    private static final int TIME_TO_WAIT_SECS = 60;

    private static final int FORK_JOIN_POOL_SIZE = 10;

    private static final String SERVER_URL = "http://crymesh2:8090";

    private GeomappingClient geomappingClient;

    @Before
    public void initialize() {

        geomappingClient = new ApiClientBuilder(SERVER_URL)
                .buildClient(GeomappingClient.class);

    }

    @Test
    public void createGeomappingsSequential() {

        List<LocationAddress> request = GeomappingRestUtil.generateRandomLocationAddresses();

        logger.info("createGeomappingsSequential() TEST request number of locations requested per client: {}", request.size());

        List<List<Geomapping>> responses = new ArrayList<>();

        ///warmup..
        geomappingClient.createGeomappings(request);

        long start = System.currentTimeMillis();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            List<Geomapping> response = geomappingClient.createGeomappings(request);
            responses.add(response);

        }

        long end  = System.currentTimeMillis();

        logger.info("createGeomappingsSequential() TEST number of client responses: {}", responses.size());
        logger.info("createGeomappingsSequential() TEST total time (millis): {}", end - start);

    }

    @Test
    public void createGeomappingsParallel() {

        List<LocationAddress> request = GeomappingRestUtil.generateRandomLocationAddresses();

        logger.info("createGeomappingsParallel() TEST request number of locations requested per client: {}", request.size());

        ///warmup..
        geomappingClient.createGeomappings(request);

        long start = System.currentTimeMillis();

        List<Future<List<Geomapping>>> responses = null;
        try {
            responses = spawnClientThreads(geomappingClient, request);
        }
        catch (Exception e) {
            logger.error("Error in createGeomappingsParallel()..", e);
            throw new ResourceAccessExternalServiceFailedException(e);
        }

        long end  = System.currentTimeMillis();

        logger.info("createGeomappingsParallel() TEST number of client responses: {}", responses.size());
        logger.info("createGeomappingsParallel() TEST total time (millis): {}", end - start);

    }

    private List<Future<List<Geomapping>>> spawnClientThreads(
            GeomappingClient targetApi, List<LocationAddress> request) throws Exception {

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "" + FORK_JOIN_POOL_SIZE);

        CountDownLatch latch = new CountDownLatch(CLIENT_COUNT);

        List<Future<List<Geomapping>>> responses = new ArrayList<>();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            Future<List<Geomapping>> response = submitGeomappingCompletableFuture(targetApi, request, latch);
            responses.add(response);

        }

        latch.await(TIME_TO_WAIT_SECS, TimeUnit.SECONDS);

        return responses;

    }

    private Future<List<Geomapping>> submitGeomappingCompletableFuture(GeomappingClient targetApi, List<LocationAddress> request,
            CountDownLatch latch) {

        return CompletableFuture

                //Use supplyAsync(..) when CompletableFuture will eventually return a result or exception..
                //Use runAsync(..) for "fire and forget.."
                .supplyAsync(() ->
                        targetApi.createGeomappings(request)
                )

                //This is a callback from the CompletableFuture once it is done - allows caller to handle result or exception..
                //Use whenCompleteAsync(..) to handle callback on a different thread, caller is not blocked..
                .whenCompleteAsync((geomappings, exx) -> {

                    try {
                        if (exx != null) {
                            //Handle exception..
                            logger.error("Error in completion stage; {}..", exx.getMessage());
                        }
                        else {
                            //Handle any steps to be executed after normal termination..
                            logger.debug("Reached completion stage with geomapping number of locations: {}..", geomappings.size());
                        }
                    }
                    finally {
                        latch.countDown();
                    }

                });

    }

}
