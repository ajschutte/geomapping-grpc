package com.mg.services.geomapping.grpc;

import com.google.common.util.concurrent.ListenableFuture;
import com.mg.exceptions.core.ResourceAccessExternalServiceFailedException;
import com.mg.services.geomapping.api.GeomappingList;
import com.mg.services.geomapping.api.GeomappingServiceGrpc;
import com.mg.services.geomapping.api.LocationAddressList;
import com.mg.services.geomapping.api.grpc.GeomappingGrpcUtil;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Andries on 10/24/17.
 */
@Ignore
public class GeomappingGrpcControllerClientTest {

    private Logger logger = LoggerFactory.getLogger(GeomappingGrpcControllerClientTest.class);

    private static final int CLIENT_COUNT = 800;

    //Client timeout config..
    private static final int TIME_TO_WAIT_SECS = 60;

    private static final int FORK_JOIN_POOL_SIZE = 10;

    private static final String SERVER_URL = "crymesh2:7565";

    private GeomappingServiceGrpc.GeomappingServiceBlockingStub geomappingServiceBlockingStub;

    private GeomappingServiceGrpc.GeomappingServiceStub geomappingServiceStub;

    private GeomappingServiceGrpc.GeomappingServiceFutureStub geomappingServiceFutureStub;

    @Before
    public void initialize() {

        ManagedChannel channel = ManagedChannelBuilder
                .forTarget(SERVER_URL)
                .usePlaintext(true) //unencrypted, default uses TLS
                .build();

        geomappingServiceBlockingStub = GeomappingServiceGrpc.newBlockingStub(channel);
        geomappingServiceFutureStub = GeomappingServiceGrpc.newFutureStub(channel);
        geomappingServiceStub = GeomappingServiceGrpc.newStub(channel);

    }

    @Test
    public void createGeomappingsBlockingSequential() throws Exception {

        LocationAddressList.Builder builder = LocationAddressList.newBuilder();
        GeomappingGrpcUtil.generateRandomLocationAddresses().forEach(builder::addLocationAddress);
        LocationAddressList request = builder.build();

        logger.info("createGeomappingsBlockingSequential() TEST request number of locations requested per client: {}",
                request.getLocationAddressCount());

        //warmup..
        geomappingServiceBlockingStub.createGeomappings(request);

        List<GeomappingList> responses = new ArrayList<>();

        long start = System.currentTimeMillis();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            GeomappingList response = geomappingServiceBlockingStub.createGeomappings(request);
            responses.add(response);

        }

        long end  = System.currentTimeMillis();

        logger.info("createGeomappingsBlockingSequential() TEST number of client responses: {}", responses.size());
        logger.info("createGeomappingsBlockingSequential() TEST total time (millis): {}", end - start);

    }

    @Test
    public void createGeomappingsNonBlockingSequential() throws Exception {

        LocationAddressList.Builder builder = LocationAddressList.newBuilder();
        GeomappingGrpcUtil.generateRandomLocationAddresses().forEach(builder::addLocationAddress);
        LocationAddressList request = builder.build();

        //warmup..
        geomappingServiceStub.createGeomappings(request, new GeomappingListObserver(new CountDownLatch(1)));

        CountDownLatch latch = new CountDownLatch(CLIENT_COUNT);

        GeomappingListObserver vo = new GeomappingListObserver(latch);

        logger.info("createGeomappingsNonBlockingSequential() TEST request number of locations requested per client: {}",
                request.getLocationAddressCount());

        long start = System.currentTimeMillis();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            geomappingServiceStub.createGeomappings(request, vo);

        }

        latch.await();

        long end  = System.currentTimeMillis();

        logger.info("createGeomappingsNonBlockingSequential() TEST number of client responses: {}", vo.size());
        logger.info("createGeomappingsNonBlockingSequential() TEST total time (millis): {}", end - start);

    }

    @Test
    public void createGeomappingsRpcFutureSequential() throws Exception {

        LocationAddressList.Builder builder = LocationAddressList.newBuilder();
        GeomappingGrpcUtil.generateRandomLocationAddresses().forEach(builder::addLocationAddress);
        LocationAddressList request = builder.build();

        logger.info("createGeomappingsRpcFutureSequential() TEST request number of locations requested per client: {}",
                request.getLocationAddressCount());

        //warmup..
        geomappingServiceFutureStub.createGeomappings(request);

        List<ListenableFuture<GeomappingList>> responses = new ArrayList<>();

        long start = System.currentTimeMillis();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            ListenableFuture<GeomappingList> response = geomappingServiceFutureStub.createGeomappings(request);
            responses.add(response);

        }

        /*
        responses.parallelStream().forEach(future -> {
            try {
                future.get();
            }
            catch (Exception e) {
                logger.error("Error...", e);
            }
        });
        */

        long end  = System.currentTimeMillis();

        logger.info("createGeomappingsRpcFutureSequential() TEST number of client responses: {}", responses.size());
        logger.info("createGeomappingsRpcFutureSequential() TEST total time (millis): {}", end - start);

    }

    @Test
    public void createGeomappingsParallel() {

        LocationAddressList.Builder builder = LocationAddressList.newBuilder();
        GeomappingGrpcUtil.generateRandomLocationAddresses().forEach(builder::addLocationAddress);
        LocationAddressList request = builder.build();

        logger.info("createGeomappingsParallel() TEST request number of locations requested per client: {}",
                request.getLocationAddressCount());

        //warmup..
        geomappingServiceFutureStub.createGeomappings(request);

        long start = System.currentTimeMillis();

        List<Future<GeomappingList>> responses = null;
        try {
            responses = spawnClientThreads(geomappingServiceFutureStub, request);
        }
        catch (Exception e) {
            logger.error("Error in createGeomappingsParallel()..", e);
            throw new ResourceAccessExternalServiceFailedException(e);
        }

        long end  = System.currentTimeMillis();

        logger.info("createGeomappingsParallel() TEST number of client responses: {}", responses.size());
        logger.info("createGeomappingsParallel() TEST total time (millis): {}", end - start);

    }

    private List<Future<GeomappingList>> spawnClientThreads(GeomappingServiceGrpc.GeomappingServiceFutureStub targetApi,
            LocationAddressList request) throws Exception {

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "" + FORK_JOIN_POOL_SIZE);

        CountDownLatch latch = new CountDownLatch(CLIENT_COUNT);

        List<Future<GeomappingList>> responses = new ArrayList<>();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            Future<GeomappingList> response = submitGeomappingCompletableFuture(targetApi, request, latch);
            responses.add(response);

        }

        latch.await(TIME_TO_WAIT_SECS, TimeUnit.SECONDS);

        return responses;

    }

    private List<Future<GeomappingList>> spawnBlockingClientThreads(GeomappingServiceGrpc.GeomappingServiceBlockingStub targetApi,
            LocationAddressList request) throws Exception {

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "" + FORK_JOIN_POOL_SIZE);

        CountDownLatch latch = new CountDownLatch(CLIENT_COUNT);

        List<Future<GeomappingList>> responses = new ArrayList<>();

        for (int i = 0; i < CLIENT_COUNT; i++) {

            Future<GeomappingList> response = submitGeomappingBlockingCompletableFuture(targetApi, request, latch);
            responses.add(response);

        }

        latch.await(TIME_TO_WAIT_SECS, TimeUnit.SECONDS);

        return responses;

    }

    private Future<GeomappingList> submitGeomappingCompletableFuture(GeomappingServiceGrpc.GeomappingServiceFutureStub targetApi,
            LocationAddressList request, CountDownLatch latch) {

        return CompletableFuture

                .supplyAsync(() -> {
                            try {
                                return targetApi.createGeomappings(request).get();
                            }
                            catch (Exception e) {
                                logger.error("Error in submitGeomappingCompletableFuture(..)..");
                                throw new ResourceAccessExternalServiceFailedException(e);
                            }

                        }
                )

                .whenCompleteAsync((geomappings, exx) -> {

                    try {
                        if (exx != null) {
                            //Handle exception..
                            logger.error("Error in completion stage; {}..", exx.getMessage());
                        }
                        else {
                            //Handle any steps to be executed after normal termination..
                            logger.debug("Reached completion stage with geomapping number of locations: {}..",
                                    geomappings.getGeomappingCount());
                        }
                    }
                    finally {
                        latch.countDown();
                    }

                });

    }

    private Future<GeomappingList> submitGeomappingBlockingCompletableFuture(GeomappingServiceGrpc.GeomappingServiceBlockingStub targetApi,
            LocationAddressList request, CountDownLatch latch) {

        return CompletableFuture

                .supplyAsync(
                        () -> targetApi.createGeomappings(request)
                )

                .whenCompleteAsync((geomappings, exx) -> {

                    try {
                        if (exx != null) {
                            //Handle exception..
                            logger.error("Error in blocking completion stage; {}..", exx.getMessage());
                        }
                        else {
                            //Handle any steps to be executed after normal termination..
                            logger.debug("Reached blocking completion stage with geomapping number of locations: {}..",
                                    geomappings.getGeomappingCount());
                        }
                    }
                    finally {
                        latch.countDown();
                    }

                });

    }

    private class GeomappingListObserver implements StreamObserver<GeomappingList> {

        private AtomicInteger counter = new AtomicInteger(0);
        private CountDownLatch latch;

        public GeomappingListObserver(CountDownLatch latch) {
            this.latch = latch;
        }

        @Override
        public void onNext(GeomappingList value) {
            counter.incrementAndGet();
            latch.countDown();
        }

        @Override
        public void onError(Throwable t) {
            logger.error("onError() TEST..", t);
            latch.countDown();
        }

        @Override
        public void onCompleted() { }

        public int size() {
            return counter.get();
        }

    }

}
